package com.german.datosusuario.service;

import com.german.datosusuario.domain.Usuario;
import com.german.datosusuario.repository.UsuarioRepository;
import java.util.List;
import java.util.NoSuchElementException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
@RequiredArgsConstructor
public class UsuarioService {
    
    private final UsuarioRepository repositorio;
    
    public List<Usuario> buscarTodos() {
        return repositorio.findAll();
    }

    public Usuario guardarUsuario(Usuario usuario) {
        Assert.notNull(usuario, "El usuario no debe ser nulo");
        Assert.hasText(usuario.getNombre(), "El usuario debe tener un nombre");
        Assert.hasText(usuario.getApellido(),"El usuario de tener un apellido");
        Assert.hasText(usuario.getPassword(),"El usuario de tener un password");
        return repositorio.save(usuario);
    }

    public void eliminarPorId(long id) throws NoSuchElementException {
        Usuario usuario = repositorio.findById(id).get();
        repositorio.deleteById(usuario.getId());
    }
    
}

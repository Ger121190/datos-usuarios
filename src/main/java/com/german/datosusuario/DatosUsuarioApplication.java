package com.german.datosusuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatosUsuarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatosUsuarioApplication.class, args);
	}

}

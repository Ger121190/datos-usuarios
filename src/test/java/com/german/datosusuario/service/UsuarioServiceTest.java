package com.german.datosusuario.service;

import com.german.datosusuario.DatosUsuarioApplicationTests;
import com.german.datosusuario.domain.Usuario;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.jdbc.JdbcTestUtils;

public class UsuarioServiceTest extends DatosUsuarioApplicationTests{
    
    @Autowired
    private UsuarioService service;
    
    @Test
    public void buscarTodos_conUsuarios_CantidadDeElementosIgualACantidadDeFilasDeTabla() {
        List<Usuario> usuarios = service.buscarTodos();
        int filasEnTabla = JdbcTestUtils.countRowsInTable(jdbcTemplate, "usuario");
        assertEquals(filasEnTabla, usuarios.size());
    } 
    
    @Test
    public void guardarUsuario_conUsuarioValido_returnUsuarioConIdNoNulo() {
        Usuario usuarioAGuardar = new Usuario();
        usuarioAGuardar.setNombre("Gabriel");
        usuarioAGuardar.setApellido("Hernandez");
        usuarioAGuardar.setPassword("12345");
        Assert.assertNotNull(service.guardarUsuario(usuarioAGuardar).getId());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void guardarUsuario_conUsuarioConNombreVacio_retornaExcepcion () {
        Usuario usuarioAGuardar = new Usuario();
        usuarioAGuardar.setNombre("");
        usuarioAGuardar.setApellido("Hernandez");
        usuarioAGuardar.setPassword("12345");
        service.guardarUsuario(usuarioAGuardar);
    }
    
   @Test(expected = IllegalArgumentException.class)
   public void guardarUsuario_conUsuarioConApellidoVacio_retornaExcepcion() {
       Usuario usuarioAGuardar = new Usuario();
        usuarioAGuardar.setNombre("Gabriel");
        usuarioAGuardar.setApellido("");
        usuarioAGuardar.setPassword("12345");
        service.guardarUsuario(usuarioAGuardar);
   }
    
   @Test(expected = IllegalArgumentException.class)
   public void guardarUsuario_conUsuarioConPasswordVacio_retornaExcepcion() {
       Usuario usuarioAGuardar = new Usuario();
        usuarioAGuardar.setNombre("Gabriel");
        usuarioAGuardar.setApellido("Hernandez");
        usuarioAGuardar.setPassword("");
        service.guardarUsuario(usuarioAGuardar);
   }
   
   @Test(expected = IllegalArgumentException.class)
   public void guardarUsuario_UsuarioNull_retornaExcepcion() {
       service.guardarUsuario(null);
   }
   
   @Test
    public void eliminarUsuario_idExistente_eliminaUsuario() {
        int filasEnTablaAntesDeEliminar = JdbcTestUtils.countRowsInTable(jdbcTemplate, "usuario");
        service.eliminarPorId(1l);
        int filasEnTablaDespuesDeEliminar = JdbcTestUtils.countRowsInTable(jdbcTemplate, "usuario");
        assertEquals(filasEnTablaDespuesDeEliminar, filasEnTablaAntesDeEliminar - 1);
    }
    
    @Test(expected = NoSuchElementException.class)
    public void eliminarPorId_idInexistente_lanzaExcepcion() {
        service.eliminarPorId(-1l);
    }
}
